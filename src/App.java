import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
       
        ejercicio_4();
        ejercicio_5();
        ejercicio_6();
        
    }
    public static void ejercicio_4(){
        try {           
            System.out.print("Por favor ingrese una velocidad en km/h: ");
            Scanner leer_velocidad=new Scanner(System.in);
            double velocidad_kh = leer_velocidad.nextDouble();
            double velocidad_ms = velocidad_kh / 3.6;
            System.out.println("La velocidad "+velocidad_kh+" km/h corresponde a "+velocidad_ms+" m/s");
            //leer_velocidad.close();


        } catch (Exception e) {
            //TODO: handle exception
            System.out.println("Ingrese un valor valido");
        }
        
    }

    public static void ejercicio_5(){
        try {
            System.out.println("Por favor ingrese el valor de los dos catetos: ");
            Scanner leer_catetos=new Scanner(System.in);
            double cateto_opuesto=leer_catetos.nextDouble();
            double cateto_adyacente=leer_catetos.nextDouble();
            double hipotenusa=Math.pow(Math.pow(cateto_opuesto,2)+Math.pow(cateto_adyacente,2),0.5);        
            System.out.println("El valor de la hipotenusa es "+hipotenusa);
            //leer_catetos.close();

        } catch (Exception e) {
            //TODO: handle exception
            System.out.println("Por favor ingrese un valor numérico");
        }
    }
    
    public static void ejercicio_6(){
        try{
            System.out.print("Por favor ingrese un número: ");
            Scanner leer_numero = new Scanner(System.in);
            double num = leer_numero.nextInt();
            String resultado = (num%10==0) ? "El número es múltimo de 10" : "El número NO es múltiplo de 10";
            System.out.println(resultado);
            //leer_numero.close();
            
            
            
        } catch (Exception e) {
            System.out.println("Por favor ingrese un valor entero");    
        }
    }
/*
    public static void ejercicio_9(){
        System.out.println("Por favor ingrese 3 valores: ");
        Scanner leer_numeros= new Scanner(System.in);
        int num_1 = leer_numeros.nextInt();
        int num_2 = leer_numeros.nextInt();
        int num_3 = leer_numeros.nextInt();
        //leer_numeros.close();

        int mayor_valor = 
    }
*/


}
